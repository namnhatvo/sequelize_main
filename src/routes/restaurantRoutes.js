import express from "express";
import { addLike, addRate, getLikeListByRestaurant, getLikeListByUser, getRateListByRestaurant, getRateListByUser, unLike } from "../controllers/restaurantController.js";

const restaurantRoute = express.Router();

restaurantRoute.post("/add-like/:userId/:resId", addLike);

restaurantRoute.delete("/un-like/:userId/:resId", unLike);

restaurantRoute.get("/get-like-list-by-user/:userId", getLikeListByUser);

restaurantRoute.get("/get-like-list-by-restaurant/:resId", getLikeListByRestaurant);

restaurantRoute.post("/add-rate/:userId/:resId", addRate);

restaurantRoute.get("/get-rate-list-by-user/:userId", getRateListByUser);

restaurantRoute.get("/get-rate-list-by-restaurant/:resId", getRateListByRestaurant);

export default restaurantRoute;
