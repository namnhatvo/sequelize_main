import express from 'express';
import restaurantRoute from './restaurantRoutes.js';
import userRoute from './userRoutes.js';

const rootRoute = express.Router();

rootRoute.use("/restaurant", restaurantRoute)
rootRoute.use("/user", userRoute )


export default rootRoute;