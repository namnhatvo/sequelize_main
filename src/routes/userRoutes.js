import express from "express";
import { addOrder } from "../controllers/userController.js";

const userRoute = express.Router();

userRoute.post("/order/:userId/:foodId", addOrder)

export default userRoute
