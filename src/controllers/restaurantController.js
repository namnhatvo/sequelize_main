import sequelize from "../models/connect.js";
import initModels from "../models/init-models.js";

let modal = initModels(sequelize);

const addLike = async (req, res) => {
  let { userId, resId } = req.params;

  let checkLike = await modal.like_res.findAll({
    where: {
      res_id: resId,
      user_id: userId,
    },
  });

  if (checkLike.length == 0) {
    let newData = {
      user_id: userId,
      res_id: resId,
      date_like: new Date(),
    };
    await modal.like_res.create(newData);
  }
  res.send(true);
};

const unLike = async (req, res) => {
  let { userId, resId } = req.params;

  await modal.like_res.destroy({
    where: {
      res_id: resId,
      user_id: userId,
    },
  });

  res.send(true);
};

const getLikeListByUser = async (req, res) => {
  let { userId } = req.params;
  let data = await modal.like_res.findAll({
    where: {
      user_id: userId,
    },
    include: ["user"],
  });
  res.send(data);
};

const getLikeListByRestaurant = async (req, res) => {
  let { resId } = req.params;
  let data = await modal.like_res.findAll({
    where: {
      res_id: resId,
    },
    include: ["re"],
  });
  res.send(data);
};

const addRate = async (req, res) => {
  let { amount } = req.body;
  let { userId, resId } = req.params;

  let newData = {
    user_id: userId,
    res_id: resId,
    amount,
    date_rate: new Date(),
  };

  await modal.rate_res.create(newData);

  res.send(true);
};

const getRateListByUser = async (req, res) => {
  let { userId } = req.params;
  let data = await modal.rate_res.findAll({
    where: {
      user_id: userId,
    },
    include: ["user"],
  });
  res.send(data);
};

const getRateListByRestaurant = async (req, res) => {
  let { resId } = req.params;
  let data = await modal.rate_res.findAll({
    where: {
      res_id: resId,
    },
    include: ["re"],
  });
  res.send(data);
};
export {
  addLike,
  unLike,
  getLikeListByUser,
  getLikeListByRestaurant,
  addRate,
  getRateListByUser,
  getRateListByRestaurant,
};
