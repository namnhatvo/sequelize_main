import sequelize from "../models/connect.js";
import initModels from "../models/init-models.js";

let modal = initModels(sequelize);

const addOrder = async (req, res) => {
  let { userId, foodId } = req.params;
  let { amount, code, arr_sub_id } = req.body;

  let checkOrdered = await modal.orders.findAll({
    where: {
      food_id: foodId,
      user_id: userId,
    },
  });

  if (checkOrdered.length == 0) {
    let newData = {
      user_id: userId,
      food_id: foodId,
      amount,
      code,
      arr_sub_id,
    };
    await modal.orders.create(newData);
    res.send(true);
  } else {
    res.send("You ordered this food!!")
  }
};

export { addOrder };
