import jwt from "jsonwebtoken";

const createToken = (data) => {
  let token = jwt.sign(
    { data },
    "NODE35",
    // { algorithm: "HS256" },
    { expiresIn: "5y" }
  );
  return token;
};

const checkToken = (token) => {
  return jwt.verify(token, "NODE35");
};

const decodeToken = (token) => {
  return jwt.decode(token);
};

export { createToken, checkToken, decodeToken };
